//khai báo thư viện express
const express = require('express');
const prizeMiddleware = require('../middlewares/prizeMiddleware');
const {
    getAllPrize,
    getPrizeById,
    createPrize,
    updatePrizeById,
    deletePrizeById
  } = require("../controllers/prizeController");
//tạo router
const prizeRouter = express.Router();

//sủ dụng middle ware
prizeRouter.use(prizeMiddleware);

//get all Prizes
prizeRouter.get('/prizes', getAllPrize);

//get a Prize
prizeRouter.get('/prizes/:prizeid', getPrizeById);

//create a Prize
prizeRouter.post('/prizes', createPrize);

//update a Prize
prizeRouter.put('/prizes/:prizeid', updatePrizeById);

//delete a Prize

prizeRouter.delete('/prizes/:prizeid', deletePrizeById);

module.exports = { prizeRouter };