//khai báo thư viện express
const express = require('express');
const { diceHistoryMiddleware } = require('../middlewares/diceHistoryMiddleware');

const {
    getAllDiceHistory,
    getDiceHistoryById,
    createDiceHistory,
    updateDiceHistoryById,
    deleteDiceHistoryById
  } = require("../controllers/diceHistoryController");
//tạo router
const diceHistoryRouter = express.Router();

 
//sủ dụng middle ware
diceHistoryRouter.use(diceHistoryMiddleware);
 
//get all dice
diceHistoryRouter.get('/dice-histories',getAllDiceHistory);

//get a dice
diceHistoryRouter.get('/dice-histories/:diceHistoryid', getDiceHistoryById);
 
//create a dice
diceHistoryRouter.post('/dice-histories', createDiceHistory);

//update a diceHistory
diceHistoryRouter.put('/dice-histories/:diceHistoryid', updateDiceHistoryById);

//delete a dice
diceHistoryRouter.delete('/dice-histories/:diceHistoryid', deleteDiceHistoryById)

module.exports = { diceHistoryRouter };