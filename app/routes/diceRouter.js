//khai báo thư viện express
const express = require('express');
const { diceMiddleware } = require('../middlewares/diceMiddleware');

const {
    diceHandler,
    getDiceHistoryByUserName,
    getPrizeHistoryByUserName,
    getVoucherHistoryByUserName
  } = require("../controllers/diceController");
//tạo router
const diceRouter = express.Router();

 
//sủ dụng middle ware
diceRouter.use(diceMiddleware);
 
 
//create a dice
diceRouter.post('/devcamp-lucky-dice/dice', diceHandler);
diceRouter.get('/devcamp-lucky-dice/dice-history', getDiceHistoryByUserName);
diceRouter.get('/devcamp-lucky-dice/prize-history', getPrizeHistoryByUserName);
diceRouter.get('/devcamp-lucky-dice/voucher-history', getVoucherHistoryByUserName);
module.exports = { diceRouter };