//khai báo thư viện express
const express = require('express');
const voucherHistoryMiddleware = require('../middlewares/voucherHistoryMiddleware');
const {
    getAllVoucherHistory  ,
    getVoucherHistoryById  ,
    createVoucherHistory  ,
    updateVoucherHistoryById  ,
    deleteVoucherHistoryById   
  } = require("../controllers/voucherHistoryController");
//tạo router
const voucherHistoryRouter = express.Router();

//sủ dụng middle ware
voucherHistoryRouter.use(voucherHistoryMiddleware);

//get all Prizes
voucherHistoryRouter.get('/voucher-histories', getAllVoucherHistory);

//get a Prize
voucherHistoryRouter.get('/voucher-histories/:historyId', getVoucherHistoryById);

//create a Prize
voucherHistoryRouter.post('/voucher-histories', createVoucherHistory);

//update a Prize
voucherHistoryRouter.put('/voucher-histories/:historyId', updateVoucherHistoryById);

//delete a Prize

voucherHistoryRouter.delete('/voucher-histories/:historyId', deleteVoucherHistoryById);

module.exports = { voucherHistoryRouter };