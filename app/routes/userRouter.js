//khai báo thư viện express
const express = require('express');
const userMiddleware = require('../middlewares/userMiddleware');
const {
    getAllUser,
    getUserById,
    createUser,
    updateUserById,
    deleteUserById
  } = require("../controllers/userController");
//tạo router
const userRouter = express.Router();

//sủ dụng middle ware
userRouter.use(userMiddleware);

//get all users
userRouter.get('/users', getAllUser);

//get a user
userRouter.get('/users/:userId', getUserById);

//create a user
userRouter.post('/users', createUser);

//update a user
userRouter.put('/users/:userId', updateUserById);

//delete a user
userRouter.delete('/users/:userId', deleteUserById);

module.exports = { userRouter };