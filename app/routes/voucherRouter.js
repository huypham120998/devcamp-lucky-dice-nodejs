//khai báo thư viện express
const express = require('express');
const voucherMiddleware = require('../middlewares/voucherMiddleware');
const {
    getAllVoucher,
    getVoucherById,
    createVoucher,
    updateVoucherById,
    deleteVoucherById
  } = require("../controllers/voucherController");
//tạo router
const voucherRouter = express.Router();

//sủ dụng middle ware
voucherRouter.use(voucherMiddleware);

//get all vouchers
voucherRouter.get('/vouchers', getAllVoucher);

//get a voucher
voucherRouter.get('/vouchers/:voucherid', getVoucherById);

//create a voucher
voucherRouter.post('/vouchers', createVoucher);

//update a voucher
voucherRouter.put('/vouchers/:voucherid',updateVoucherById);

//delete a voucher 
voucherRouter.delete('/vouchers/:voucherid', deleteVoucherById);

module.exports = { voucherRouter };