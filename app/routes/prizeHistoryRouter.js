//khai báo thư viện express
const express = require('express');
const prizeHistoryMiddleware = require('../middlewares/prizeHistoryhMiddleware');
const {
    getAllPrizeHistory ,
    getPrizeHistoryById ,
    createPrizeHistory ,
    updatePrizeHistoryById ,
    deletePrizeHistoryById  
  } = require("../controllers/prizeHistoryController");
//tạo router
const prizeHistoryRouter = express.Router();

//sủ dụng middle ware
prizeHistoryRouter.use(prizeHistoryMiddleware);

//get all Prizes
prizeHistoryRouter.get('/prize-histories', getAllPrizeHistory);

//get a Prize
prizeHistoryRouter.get('/prize-histories/:historyId', getPrizeHistoryById);

//create a Prize
prizeHistoryRouter.post('/prize-histories', createPrizeHistory);

//update a Prize
prizeHistoryRouter.put('/prize-histories/:historyId', updatePrizeHistoryById);

//delete a Prize

prizeHistoryRouter.delete('/prize-histories/:historyId', deletePrizeHistoryById);

module.exports = { prizeHistoryRouter };