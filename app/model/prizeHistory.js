const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const PrizeHistory = new Schema({
	// _id: {type: ObjectId, unique:true},
    user: {type: mongoose.Types.ObjectId, ref:"User", required: true},
    prize: {type: mongoose.Types.ObjectId, ref:"Prize", required: true},
},{
	timestamps:true
});

module.exports = mongoose.model('PrizeHistory', PrizeHistory);
 