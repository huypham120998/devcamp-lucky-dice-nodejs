const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const VoucherHistory = new Schema({
	// _id: {type: ObjectId, unique:true},
    user: {type: mongoose.Types.ObjectId, ref:"User", required: true},
    voucher: {type: mongoose.Types.ObjectId, ref:"Voucher", required: true},
},{
	timestamps:true
});

module.exports = mongoose.model('VoucherHistory', VoucherHistory);
 