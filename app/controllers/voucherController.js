const voucherModel = require('../model/voucher');
//import thư viện mongoose
const mongoose = require('mongoose');

const getAllVoucher = (req, res) => {
  //B1: Chuẩn bị dữ liệu
  
  //B2: Validate dữ liệu
  //B3: Thao tác với cơ sở dữ liệu
  voucherModel.find((error, data) => {
    if (error) {
      return res.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return res.status(200).json({
        status: "Success: Get all voucher success",
        data: data
      })
    }
  })
}

const createVoucher = (req, res) => {
  //B1: thu thập dữ liệu từ req
  let body = req.body;
  // console.log(body);
  //B2: validate dữ liệu
  if (!body.code) {
    return res.status(400).json({
      message: 'Mã voucher phải nhập'
    })
  }
  if (!Number.isInteger(body.discount) || body.discount < 0) {
    return res.status(400).json({
      message: 'Phần trăm giảm giá không hợp lệ'
    })
  }
  //B3: Gọi model thực hiện các thao tác nghiệp vụ
  let newVoucherData = {
    _id: mongoose.Types.ObjectId(),
    code: body.code,
    discount: body.discount,
    note: body.note
  }
  voucherModel.create(newVoucherData, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }

    return res.status(201).json({
      message: "Create successfully",
      newVoucher: data
    })
  })
}

const getVoucherById = (req, res) => {
  //B1: thu thập dữ liệu từ req
  let id = req.params.voucherid;
  //B2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).json({
      message: 'VoucherId is invalid!'
    })
  }
  //B3: Gọi model thực hiện các thao tác nghiệp vụ
  voucherModel.findById(id, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }
    if (data === null) return res.status(404).json({
      notFound: "ID not found!"
    })
    return res.status(200).json({
      message: "Get voucher successfully",
      voucher: data
    })
  })
}


const updateVoucherById = (req, res) => {
  //B1: thu thập dữ liệu từ req
  let id = req.params.voucherid;
  let body = req.body;

  //B2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).json({
      message: 'VoucherId is invalid!'
    })
  }
  //bóc tách trường hợp undefined
  if (body.code !== undefined && body.code == "") {
    return res.status(400).json({
      message: 'Mã voucher phải nhập'
    })
  }
  if (body.discount !== undefined && (!Number.isInteger(body.discount) || body.discount < 0)) {
    return res.status(400).json({
      message: 'Phần trăm giảm giá không hợp lệ'
    })
  }
  //B3: Gọi model thực hiện các thao tác nghiệp vụ
  let voucherUpdate = {
    code: body.code,
    discount: body.discount,
    note: body.note
  }
  if (body.code) {
    voucherUpdate.code = body.code;
  }
  if (body.discount) {
    voucherUpdate.discount = body.discount;
  }
  if (body.note) {
    voucherUpdate.note = body.note;
  }

  voucherModel.findByIdAndUpdate(id, voucherUpdate, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }

    return res.status(200).json({
      message: "Update voucher successfully",
      voucher: data
    })
  })
}

const deleteVoucherById = (req, res) => {
  //B1: thu thập dữ liệu từ req
  let id = req.params.voucherid;
  //B2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).json({
      message: 'VoucherId is invalid!'
    })
  }
  //B3: Gọi model thực hiện các thao tác nghiệp vụ
  voucherModel.findByIdAndDelete(id, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }
    if (data === null) return res.status(404).json({
      notFound: "ID not found!"
    })
    return res.status(204).json({
      message: "Delete voucher successfully",
    })
  })
}

module.exports = {
  getAllVoucher,
  getVoucherById,
  createVoucher,
  updateVoucherById,
  deleteVoucherById
}