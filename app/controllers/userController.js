const userModel = require('../model/user');
//import thư viện mongoose
const mongoose = require('mongoose');

const getAllUser = (req, res) => {
  //B1: Chuẩn bị dữ liệu
  //B2: Validate dữ liệu
  //B3: Thao tác với cơ sở dữ liệu
  userModel.find((error, data) => {
    if (error) {
      return res.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return res.status(200).json({
        status: "Success: Get courses success",
        data: data
      })
    }
  })
}

const createUser = (req, res) => {
  //B1: thu thập dữ liệu từ req
  let body = req.body;
  // console.log(body);
  //B2: validate dữ liệu
  if (!body.username) {
    return res.status(400).json({
      message: 'username is required'
    })
  }
  if (!body.firstname) {
    return res.status(400).json({
      message: 'firstname is required'
    })
  }
  if (!body.lastname) {
    return res.status(400).json({
      message: 'lastname is required'
    })
  }
  //B3: Gọi model thực hiện các thao tác nghiệp vụ
  let newUserData = {
    _id: mongoose.Types.ObjectId(),
    username: body.username,
    firstname: body.firstname,
    lastname: body.lastname,
  }
  userModel.create(newUserData, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }

    return res.status(201).json({
      message: "Create successfully",
      newUser: data
    })
  })
}
const getUserById = (req, res) => {
  //B1: Chuẩn bị dữ liệu
  let userId = req.params.userId;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return res.status(400).json({
      status: "Error 400: Bad Request",
      message: "Users ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  userModel.findById(userId, (error, data) => {
    if (error) {
      return res.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return res.status(200).json({
        status: "Success: Get user success",
        data: data
      })
    }
  })
}

const updateUserById = (req, res) => {
  //B1: Chuẩn bị dữ liệu
  let userId = req.params.userId;
  let bodyRequest = req.body;

  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return res.status(400).json({
      status: "Error 400: Bad Request",
      message: "User ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  let userNewUpdate = {
    username: bodyRequest.username,
    firstname: bodyRequest.firstname,
    lastname: bodyRequest.lastname
  }

  userModel.findByIdAndUpdate(userId, userNewUpdate, (error, data) => {
    if (error) {
      return res.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return res.status(200).json({
        status: "Success: Update review success",
        data: data
      })
    }
  })
}

const deleteUserById = (req, res) => {

  //B1: thu thập dữ liệu từ req
  let userId = req.params.userId;
  //B2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return res.status(400).json({
      message: 'UserId is invalid!'
    })
  }
  //B3: Gọi model thực hiện các thao tác nghiệp vụ
  userModel.findByIdAndDelete(userId, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }
    if (data === null) return res.status(404).json({
      notFound: "ID not found!"
    })
    return res.status(204).json({
      message: "Delete user successfully",
    })
  })
}

module.exports = {
  getAllUser,
  getUserById,
  createUser,
  updateUserById,
  deleteUserById
}