const voucherHistoryModel = require('../model/voucherHistory');

//import thư viện mongoose
const mongoose = require('mongoose');
const getAllVoucherHistory  = (req, res) => {
  //B1: Chuẩn bị dữ liệu
  let { user } = req.query;

  let condition = {};

  if(user){
      condition.user = user;
  }
  //B2: Validate dữ liệu
  //B3: Thao tác với cơ sở dữ liệu
  voucherHistoryModel.find(condition, (error, data) => {
    if (error) {
      return res.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return res.status(200).json({
        status: "Success: Get all Voucher History success",
        data: data
      })
    }
  })
}

const createVoucherHistory = (req, res) => {
  //B1: thu thập dữ liệu từ req
  let body = req.body;
  //B2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(body.user)) {
    return res.status(400).json({
      message: 'User is invalid!'
    })
  }
  if (!mongoose.Types.ObjectId.isValid(body.voucher)) {
    return res.status(400).json({
      message: 'Voucher is invalid!'
    })
  }
  //B3: Gọi model thực hiện các thao tác nghiệp vụ
  let newVoucherHistoryData = {
    _id: mongoose.Types.ObjectId(),
    user: body.user,
    voucher: body.voucher,
  }
  voucherHistoryModel.create(newVoucherHistoryData, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }
    return res.status(201).json({
      message: "Create successfully",
      newVoucherHistory: data
    })
  })
}

const getVoucherHistoryById  = (req, res) => {
  // console.log(req.params.Prizeid);
  //B1: thu thập dữ liệu từ req
  let id = req.params.historyId;
  //B2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).json({
      message: 'history Id is invalid!'
    })
  }
  voucherHistoryModel.findById(id)
    .then((Prize) => res.status(200).json({
      message: "Get Voucher History successfully",
      Prize
    }))
    .catch(error => {
      res.status(500).json({
        message: error.message
      })
    });
}


const updateVoucherHistoryById  = (req, res) => {
  //B1: thu thập dữ liệu từ req
  let id = req.params.historyId;
  let body = req.body;

  //B2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(body.user)) {
    return res.status(400).json({
      message: 'User is invalid!'
    })
  }
  if (!mongoose.Types.ObjectId.isValid(body.voucher)) {
    return res.status(400).json({
      message: 'Voucher is invalid!'
    })
  }
  //B3: Gọi model thực hiện các thao tác nghiệp vụ
  let voucherHistoryUpdate = {
    user: body.user,
    prize: body.voucher
  }
  voucherHistoryModel.findByIdAndUpdate(id, voucherHistoryUpdate, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }

    return res.status(200).json({
      message: "Update Voucher History successfully",
      VoucherHistory: data
    })
  })
}

const deleteVoucherHistoryById  = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  let id = req.params.historyId;

  // B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
          message: "HistoryId is invalid!"
      })
  }

  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  voucherHistoryModel.findByIdAndDelete(id, (error, data) => {
      if (error) {
          return res.status(500).json({
              message: "error.message"
          })
      }
      else{
        return res.status(204).json({
          message: "Delete Voucher History successfully",
       })
      }
      
  })
}


// Export customer controller thành 1 module
module.exports = {
    getAllVoucherHistory  ,
    getVoucherHistoryById  ,
    createVoucherHistory  ,
    updateVoucherHistoryById  ,
    deleteVoucherHistoryById   
}