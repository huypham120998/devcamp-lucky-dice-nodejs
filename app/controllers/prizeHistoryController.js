const prizeHistoryModel = require('../model/prizeHistory');

//import thư viện mongoose
const mongoose = require('mongoose');
const getAllPrizeHistory  = (req, res) => {
  //B1: Chuẩn bị dữ liệu
  let { user } = req.query;

  let condition = {};

  if(user){
      condition.user = user;
  }
  //B2: Validate dữ liệu
  //B3: Thao tác với cơ sở dữ liệu
  prizeHistoryModel.find(condition, (error, data) => {
    if (error) {
      return res.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return res.status(200).json({
        status: "Success: Get all Prize History success",
        data: data
      })
    }
  })
}

const createPrizeHistory = (req, res) => {
  //B1: thu thập dữ liệu từ req
  let body = req.body;
  //B2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(body.user)) {
    return res.status(400).json({
      message: 'User is invalid!'
    })
  }
  if (!mongoose.Types.ObjectId.isValid(body.prize)) {
    return res.status(400).json({
      message: 'Prize is invalid!'
    })
  }
  //B3: Gọi model thực hiện các thao tác nghiệp vụ
  let newPrizeHistoryData = {
    _id: mongoose.Types.ObjectId(),
    user: body.user,
    prize: body.prize,
  }
  prizeHistoryModel.create(newPrizeHistoryData, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }
    return res.status(201).json({
      message: "Create successfully",
      newPrizeHistory: data
    })
  })
}

const getPrizeHistoryById  = (req, res) => {
  // console.log(req.params.Prizeid);
  //B1: thu thập dữ liệu từ req
  let id = req.params.historyId;
  //B2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).json({
      message: 'historyId is invalid!'
    })
  }
  prizeHistoryModel.findById(id)
    .then((Prize) => res.status(200).json({
      message: "Get Prize History successfully",
      Prize
    }))
    .catch(error => {
      res.status(500).json({
        message: error.message
      })
    });
}


const updatePrizeHistoryById  = (req, res) => {
  //B1: thu thập dữ liệu từ req
  let id = req.params.historyId;
  let body = req.body;

  //B2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(body.user)) {
    return res.status(400).json({
      message: 'User is invalid!'
    })
  }
  if (!mongoose.Types.ObjectId.isValid(body.prize)) {
    return res.status(400).json({
      message: 'Prize is invalid!'
    })
  }
  //B3: Gọi model thực hiện các thao tác nghiệp vụ
  let PrizeHistoryUpdate = {
    user: body.user,
    prize: body.prize
  }
  prizeHistoryModel.findByIdAndUpdate(id, PrizeHistoryUpdate, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }

    return res.status(200).json({
      message: "Update Prize History successfully",
      Prize: data
    })
  })
}

const deletePrizeHistoryById  = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  let id = req.params.historyId;

  // B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
          message: "HistoryId is invalid!"
      })
  }

  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  prizeHistoryModel.findByIdAndDelete(id, (error, data) => {
      if (error) {
          return res.status(500).json({
              message: "error.message"
          })
      }
      else{
        return res.status(204).json({
          message: "Delete Prize History successfully",
       })
      }
      
  })
}


// Export customer controller thành 1 module
module.exports = {
    getAllPrizeHistory ,
    getPrizeHistoryById ,
    createPrizeHistory ,
    updatePrizeHistoryById ,
    deletePrizeHistoryById  
}