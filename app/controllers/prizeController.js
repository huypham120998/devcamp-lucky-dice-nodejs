const prizeModel = require('../model/prize');

//import thư viện mongoose
const mongoose = require('mongoose');
const getAllPrize = (req, res) => {
  //B1: Chuẩn bị dữ liệu
  //B2: Validate dữ liệu
  //B3: Thao tác với cơ sở dữ liệu
  prizeModel.find((error, data) => {
    if (error) {
      return res.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return res.status(200).json({
        status: "Success: Get all order success",
        data: data
      })
    }
  })
}

const createPrize = (req, res) => {
  //B1: thu thập dữ liệu từ req
  let body = req.body;
  //B2: validate dữ liệu
  if (!body.name) {
    return res.status(400).json({
      message: 'name is required'
    })
  }

  //B3: Gọi model thực hiện các thao tác nghiệp vụ
  let newPrizeData = {
    _id: mongoose.Types.ObjectId(),
    name: body.name,
    description: body.description,
  }
  prizeModel.create(newPrizeData, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }
    return res.status(201).json({
      message: "Create successfully",
      newPrize: data
    })
  })
}

const getPrizeById = (req, res) => {
  // console.log(req.params.Prizeid);
  //B1: thu thập dữ liệu từ req
  let id = req.params.prizeid;
  //B2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).json({
      message: 'PrizeId is invalid!'
    })
  }
  prizeModel.findById(id)
    .then((Prize) => res.status(200).json({
      message: "Get Prize successfully",
      Prize
    }))
    .catch(error => {
      res.status(500).json({
        message: error.message
      })
    });
}


const updatePrizeById = (req, res) => {
  //B1: thu thập dữ liệu từ req
  let id = req.params.prizeid;
  let body = req.body;

  //B2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).json({
      message: 'PrizeId is invalid!'
    })
  }
  //bóc tách trường hợp undefined
  if (body.name !== undefined && body.name == "") {
    return res.status(400).json({
      message: 'name is required'
    })
  }
  //B3: Gọi model thực hiện các thao tác nghiệp vụ
  let PrizeUpdate = {
    name: body.name,
    description: body.description,
  }
  if (body.name) {
    PrizeUpdate.name = body.name;
  }
  if (body.description) {
    PrizeUpdate.description = body.description;
  }
  prizeModel.findByIdAndUpdate(id, PrizeUpdate, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }

    return res.status(200).json({
      message: "Update Prize successfully",
      Prize: data
    })
  })
}

const deletePrizeById = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  let prizeid = req.params.prizeid;

  // B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(prizeid)) {
      return res.status(400).json({
          message: "Prize ID is invalid!"
      })
  }

  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  prizeModel.findByIdAndDelete(prizeid, (error, data) => {
      if (error) {
          return res.status(500).json({
              message: "error.message"
          })
      }
      else{
        return res.status(204).json({
          message: "Delete Product Type successfully",
       })
      }
      
  })
}


// Export customer controller thành 1 module
module.exports = {
  getAllPrize,
  getPrizeById,
  createPrize,
  updatePrizeById,
  deletePrizeById
}