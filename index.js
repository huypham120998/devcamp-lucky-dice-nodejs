const express = require("express"); // Tương tự : import express from "express";
const { diceHistoryRouter } = require('./app/routes/diceHistoryRouter');
const { voucherRouter } = require('./app/routes/voucherRouter');
const { userRouter } = require('./app/routes/userRouter');
const { prizeRouter } = require('./app/routes/prizeRouter');
const { prizeHistoryRouter } = require('./app/routes/prizeHistoryRouter');
const { voucherHistoryRouter } = require('./app/routes/voucherHistoryRouter');
const { diceRouter } = require('./app/routes/diceRouter');
const path = require("path");

//khởi tạo ứng dụng nodejs
const app = new express();
//sử dụng được body json
app.use(express.json());

app.use(express.static(__dirname + '/views'));
app.use(express.static(__dirname + '/public'));
//sử dụng body unicode
app.use(express.urlencoded({
    extended:true
}))


const port = 8000;
const db = require('./config/db');

//Connect to DB
db.connect();
//sử dụng router

app.get("/", (request, response) => {
  // console.log(__dirname);
  response.sendFile(path.join(__dirname + "/views/luckyDice.html"))
})

app.get("/random-number", (request, response) => {
  response.status(200).json({
        randomNumber: Math.floor(Math.random() * 6 + 1)
       })
    })

//sử dụng router
app.use('/', diceHistoryRouter);
app.use('/', voucherRouter);
app.use('/', userRouter);
app.use('/', prizeRouter);
app.use('/', prizeHistoryRouter);
app.use('/', voucherHistoryRouter);
app.use('/', diceRouter);
app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})